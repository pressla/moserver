//imitrix GmbH, Alex Pressl
//client side agent sample


igroup = myBlackBox.addGroup ("Longterm", "100ms");

myBlackBox.addTimeSeries (igroup, "InletTemperature", "°C");
myBlackBox.addTimeSeries (igroup, "OutletTemperature", "°C");
myBlackBox.addTimeSeries (igroup, "InletPressure", "mbar");



//Startup Script

myBlackBox = addServer ("imitrix.cloudapp.net");

myBlackBox.addAgent ("Machine1", "1s", "RecordingAgent");
myBlackBox.addAgent ("Machine2", "1s", "RecordingAgent");
myBlackBox.addAgent ("Machine3", "1s", "RecordingAgent");
myBlackBox.addAgent ("Machine4", "1s", "RecordingAgent");
myBlackBox.addAgent ("Machine5", "1s", "RecordingAgent");




