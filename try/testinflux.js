var ire = require('influx');


var backMilliseconds = 86000 * 1000;
var startTime = new Date() - backMilliseconds;
var timeInterval = 60 * 1000;
var eventTypes = ["click", "view", "post", "comment"];

console.log("date: "+startTime);
var cpuSeries = {
  name:    "cpu_idle",
  columns: ["time", "value", "hostName"],
  points:  []
};

var eventSeries = {
  name:    "customer_events",
  columns: ["time", "customerId", "type"],
  points:  []
};


//var client = InfluxDB("imitrix.cloudapp.net", 8086, root, root, "FQ");
var client = InfluxDB.getClient({
  // or single-host configuration
  host : 'imitrix.cloudapp.net',
  port : 8086, // optional, default 8086
  username : 'root',
  password : 'root',
  database : 'FQ'
});



var eventTypes = ["click", "view", "post", "comment"];

var cpuSeries = {
  name:    "cpu_idle",
  columns: ["time", "value", "hostName"],
  points:  []
};

var eventSeries = {
  name:    "customer_events",
  columns: ["time", "customerId", "type"],
  points:  []
};

for (i = 0; i < backMilliseconds; i += timeInterval) {
  // generate fake cpu idle host values
  var hostName = "server" + Math.floor(Math.random() * 100);
  var value = Math.random() * 100;
  var pointValues = [startTime + i, value, hostName];
  cpuSeries.points.push(pointValues);

  // generate some fake customer events
  for (j = 0; j < Math.random() * 10; j += 1) {
    var customerId = Math.floor(Math.random() * 1000);
    var eventTypeIndex = Math.floor(Math.random() * 1000 % 4);
    var eventValues = [startTime + i, customerId, eventTypes[eventTypeIndex]];
    eventSeries.points.push(eventValues);
  }
}


client.writeSeries("FQ",[cpuSeries, eventSeries],0,function(err){

console.log(err);
});
