//imitrix GmbH, Alex Pressl
//MOSERVO Server Side Agent
//Pulls machine file and pushes to influxDB

var fs = require('fs');
var request = require('request');

outputFilename = "./getty.txt";
var out ="";
influxPayload = [
  {
    name: 		"loo9",
	
    columns: 	["time"],
    points: 	[
      [1400425947368, 1, "this line is first"],
      [1400425947368, 2, "and this is second"]
    ]
  }
];
var pts = [];
payload = "";

var options = {
  url: "http://192.168.0.17/bb/FQ_20141018_1_001.csv",
  method: 'GET',
  body: payload,
};

request(
    options,
    function (error, response, body) {
        //console.log(body);
		console.log(JSON.stringify(influxPayload[0]));
		lines = body.split("\n");
		influxPayload[0].points = [];
		var sequence_no = 1;
		//lines = lines.slice(0,1000);
		cheat = 1;ms=0;
		for (i in lines) {
			items = lines[i].split("\t");
			
			if (i<=1) {				//file header, line 0 = channelname, 1 = unit
				if (i==0) {			//only channel names
					for (k in items) {
						txt = items[k];	//header name or unit
						if (txt != "TIME" && txt != "TMS")
							influxPayload[0].columns.push(txt.replace("\r","")+cheat++);
					}
				console.log(JSON.stringify(influxPayload, null, 4));
				}
			}
			else {					//values
				var val = -2, timestamp = -1;
				timestamp  = parseInt(items[0]);				//first is parameter id
				ms = parseInt(items[1]);
				if (timestamp != 0)
					pts.push(timestamp*1000+ms);
				for (k in items) {
					
					if (timestamp != 0 && k>1) {
						val = parseFloat(items[k]);
						pts.push(val);
					}
				}
			if (pts.length > 0) 
				influxPayload[0].points.push(pts);
			}
			pts = [];
		}
		influxPayload[0].points.pop();
		
        if (!error && response.statusCode == 200) {
			ok = sendToInflux(JSON.stringify(influxPayload));
/*			fs.writeFile(outputFilename, JSON.stringify(influxPayload, null, 4), function(err) {
				if(err) {
				  console.log("fs.err : "+err);
				} else {
				  console.log("JSON saved to " + outputFilename);
				}
			}); 
*/
        }
    }

);
// POST all data to influx server 
function sendToInflux(logarray) {
	var postoptions = {
	  url: "http://imitrix.cloudapp.net:8086/db/F/series?u=root&p=root",
	  method: 'POST',
	  body: logarray,
	};
		console.log("POST: ");
		console.log((logarray));
		
	request(
		postoptions,
		function (error, response, body) {
			console.log("POST res: "+body);
			if (!error && response.statusCode == 200) {
				console.log("POST ok: ");
				return true;
			}
			else
				return false;
			
		}
	);
};
